var path = require("path");
module.exports = {
  devServer: {
    port: 80
  },
  css: {
    loaderOptions: {
      sass: {
      },
      postcss: {
        config: { path: path.resolve(__dirname, "./postcss.config.js") }
      }
    }
  },
  configureWebpack: {
    node: {
      fs: 'empty'
    }
  },

  lintOnSave: undefined
};
