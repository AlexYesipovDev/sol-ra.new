import Vue from "vue";
import Router from "vue-router";
import store from './store';

const Navigation  = () => import("@/components/Navigation");
const CurrentInfluence = () => import("./pages/CurrentInfluence.vue");
const Transits = () => import("./pages/Transits.vue");
const MomentsRanking = () => import("./pages/MomentsRanking.vue");
const Radix = () => import("./pages/Radix.vue");
const History = () => import("./pages/History.vue");
const Intervals = () => import("./pages/Intervals.vue");
const Data = ()=> import("./pages/DataThird.vue");
const LogInAdmin =()=> import("./pages/LogInAdmin.vue");
const Admin =()=>import("./pages/Admin.vue")


Vue.use(Router);

const defaultLayout = component => {
  return {
    default: component,
    navigation: Navigation
  }
}

const router =  new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      components: defaultLayout(Radix)
    },
    {
      path: "/moments_ranking",
      name: "moments_ranking",
      components: defaultLayout(MomentsRanking)
    },
    {
      path: "/transits",
      name: "transits",
      components: defaultLayout(Transits)
    },
    {
      path: "/current_influence",
      name: "current_influence",
      components: defaultLayout(CurrentInfluence)
    },
    {
      path: "/history",
      name: "History",
      components: defaultLayout(History)
    },
    {
      path: "/intervals",
      name: "intervals",
      components: defaultLayout(Intervals)
    }, {
      path: "/data",
      name: "data",
      components: defaultLayout(Data)
    },{
      path: "/login-admin",
      name: "login-admin",
      components: defaultLayout(LogInAdmin)
    },{
      path: "/admin",
      name: "admin",
      components: defaultLayout(Admin)
    },
  ]
})

export default router;
