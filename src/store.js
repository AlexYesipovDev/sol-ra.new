import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    data_is_loaded: false
  },
  mutations: {
    setIsLoadedParam(state, is_loaded) {
      state.data_is_loaded = is_loaded
    }
  },
  actions: {
    setIsLoadedParam({commit}, is_loaded) {
      commit('setIsLoadedParam', is_loaded)
    },
  }
});
