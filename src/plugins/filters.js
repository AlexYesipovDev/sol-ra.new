import Vue from "vue";
import { romanize, capitalize } from "./lib.js";
import * as moment from "moment";

Vue.filter("romanize", function(value) {
  return romanize(value);
});

Vue.filter("remove_seconds", function(value) {
  return value.slice(0, value.indexOf("'"));
});

Vue.filter("formatDegree", function(value) {
  value = parseFloat(value);
  let sign = '';
  if (value < 0) {
    sign = '-';
    value = -value;
  }
  let degrees = Math.floor(value);
  let minutesTemp = value - degrees;
  minutesTemp = 60.0 * minutesTemp;
  let minutes = Math.floor(minutesTemp);
  let secondsTemp = minutesTemp - minutes;
  secondsTemp = 60.0 * secondsTemp;
  let seconds = Math.round(secondsTemp);
  return sign + degrees + "°" + minutes + "'" + seconds + '"';
});

Vue.filter("formatDegreeZodiac", function(value) {
  value = parseFloat(value);
  let degrees = Math.floor(value);
  let minutesTemp = value - degrees;
  minutesTemp = 60.0 * minutesTemp;
  let minutes = Math.floor(minutesTemp);
  let secondsTemp = minutesTemp - minutes;
  secondsTemp = 60.0 * secondsTemp;
  let seconds = Math.round(secondsTemp);
  let signs = ['Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn', 'Aquarius', 'Pisces'];

  return (degrees % 30) + "°" + minutes + "'" + seconds + '" ' + signs[Math.floor(degrees / 30)];
});

Vue.filter("autoFormat", function(value) {
  if (typeof value == 'number')
    if (Math.floor(value) === value)
      return moment.unix(value).utc().format('DD.MM.YYYY HH:mm');
  return value;
});

Vue.filter("timestampToDate", function(value) {
  return moment(value * 1000).utc().format("DD.MM.YYYY HH:mm")
})

Vue.filter("fromUnixToDate", function(value) {

})

Vue.filter("capitalize", function(value) {
  return capitalize(value);
});

Vue.filter("cap", function(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
});

Vue.filter('toFixed', function(val, number) {
  return val.toFixed(number);
})

export default Vue;
