import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Buefy from "buefy";
import VeeValidate from "vee-validate";
import vClickOutside from "v-click-outside";
import VueMask from "v-mask";
import * as VueGoogleMaps from "vue2-google-maps";
import VueScrollTo from 'vue-scrollto'
import "@fortawesome/fontawesome-free/css/all.css";
import "./plugins/filters";
import SortedTablePlugin from "vue-sorted-table";
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import VueLodash from 'vue-lodash'


const options = { name: 'lodash' };


Vue.use(VueLodash, options);

Vue.use(Buefy, {
    defaultIconPack: "fas",
});
Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyAB1ht0gAqEpBoaC_c-NO1MaAB8dcAsGms",
        libraries: "places"
    }
});
Vue.use(SortedTablePlugin);
Vue.use(VeeValidate);
Vue.use(VueMask);
Vue.use(vClickOutside);
Vue.use(VueScrollTo);
Vue.use(VueMaterial);




new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
